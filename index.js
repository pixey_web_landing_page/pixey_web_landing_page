const express = require('express');
const path = require('path');
const http = require('http');
const app = express();

const server = http.createServer(app);

app.use(express.static(path.join(__dirname, '/public')));

app.get('/', function(req, res) {
   res.sendFile(__dirname + '/public/index.html');
 });



const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
   console.log('Server listening at ', PORT);
})